package ast.visitor;

import ast.CP.*;
import ast.CP.*;

import ast.CP.Cp_App_Expression;
import ast.CP.cpSwitchCaseExpression;
import ast.CP.cpSwitchExpression;
import ast.nodes.*;
import ast.nodes.AnyName;
import ast.CP.FunctionExpression;
import com.sun.security.jgss.GSSUtil;
import generated.HTMLParser;
import generated.HTMLParserBaseVisitor;
import org.antlr.v4.codegen.model.chunk.ArgRef;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.w3c.dom.ls.LSOutput;

import javax.swing.*;
import java.lang.Object;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BaseVisitor extends HTMLParserBaseVisitor {

    @Override
    public HtmlDocument visitHtmlDocument(HTMLParser.HtmlDocumentContext ctx) {
        HtmlDocument htmlDocument = new HtmlDocument();
        for (int i = 0; i < ctx.htmlElements().size(); i++) {
            htmlDocument.getElements().add(visitHtmlElements(ctx.htmlElements(i)));
        }
        for (int i = 0; i < ctx.scriptletOrSeaWs().size(); i++) {
            htmlDocument.getScriptletOrSeaWs().add(visitScriptletOrSeaWs(ctx.scriptletOrSeaWs(i)));
        }
        if (ctx.DTD() != null)
            htmlDocument.setDTD(ctx.DTD().toString());
        return htmlDocument;
    }

    @Override
    public ScriptletOrSeaWs visitScriptletOrSeaWs(HTMLParser.ScriptletOrSeaWsContext ctx) {
        ScriptletOrSeaWs scriptletOrSeaWs = new ScriptletOrSeaWs();
        if (ctx.SCRIPTLET() != null && ctx.SEA_WS() != null) {
            scriptletOrSeaWs.setSCRIPTLET(ctx.SCRIPTLET().toString());
            scriptletOrSeaWs.setSEA_WS(ctx.SEA_WS().toString());
        }
        return scriptletOrSeaWs;
    }

    @Override
    public HTMLElement visitHtmlElements(HTMLParser.HtmlElementsContext ctx) {
        HTMLElement htmlElements = new HTMLElement();
        List<HtmlMisc> htmlMiscs = new ArrayList<>();
        if (ctx != null)
            for (int i = 0; i < ctx.htmlMisc().size(); i++) {

                if (ctx.htmlMisc().get(i) != null) {
                    htmlMiscs.add(visitHtmlMisc(ctx.htmlMisc(i)));

                }
            }
        htmlElements.setHtmlMisc(htmlMiscs);
        htmlElements.setHtmlElement(visitHtmlElement(ctx.htmlElement()));
        return htmlElements;
    }


    @Override
    public HTMLElement visitHtmlElement(HTMLParser.HtmlElementContext ctx) {
        HTMLTag tag = new HTMLTag();
        tag.setName(ctx.TAG_NAME(0).getText());
        System.out.println("Element (" + tag.getName() + ")");
//            System.out.print("\t");
        System.out.println("\tname:\t" + tag.getName());
        if (ctx.htmlAttribute().size() > 0) {
            System.out.println("\tAttributes :");
            List<Attribute> attributeList = new ArrayList<>();
            for (int j = 0; j < ctx.htmlAttribute().size(); j++) {
                attributeList.add((Attribute) visitHtmlAttribute(ctx.htmlAttribute(j)));
                tag.setAttributes(attributeList);
            }
        }
        if (ctx.htmlContent() != null) {

            if (ctx.htmlContent().htmlElement().size() <= 0) { // Html tag
                if (ctx.htmlContent().getChildCount() > 0) {
                    tag.setTextContent(ctx.htmlContent().htmlChardata(0).getText());
                    System.out.println("\t\tContent: " + ctx.htmlContent().htmlChardata(0).getText());
                }
                return tag;
                //TODO iterate on ctx.htmlAttribute() and generate attribute then set it tag
            } else if (ctx.htmlContent().htmlElement().size() > 0) {

                List<HTMLElement> elements = new ArrayList<HTMLElement>();
                for (int i = 0; i < ctx.htmlContent().htmlElement().size(); i++) {
                    System.out.println("\tElements: {");
                    elements.add(visitHtmlElement(ctx.htmlContent().htmlElement(i)));

                }
                tag.setContent(elements);
                // tag.getHtmlElement()
                return tag;
            }
        }
        return tag;
    }


    @Override
    public HtmlContent visitHtmlContent(HTMLParser.HtmlContentContext ctx) {

        HtmlContent htmlContent = new HtmlContent();
        if (ctx.htmlChardata() != null)
            htmlContent.setHtmlChardata(visitHtmlChardata(ctx.htmlChardata(0)));
        htmlContent.setHtmlComment(visitHtmlComment(ctx.htmlComment(0)));
        htmlContent.setHtmlElement(visitHtmlElement(ctx.htmlElement(0)));
        htmlContent.setCDATA(ctx.CDATA().toString());
        return htmlContent;
    }


    @Override
    public HtmlAttribute visitHtmlAttribute(HTMLParser.HtmlAttributeContext ctx) {
        HtmlAttribute htmlAttribute = new HtmlAttribute();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).getText().equals("cp-app")) {
                htmlAttribute.setCP_APP(ctx.CP_APP().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_APP());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCp_app_expression(visitCp_app_expression(ctx.cp_app_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());

                //break;
            }
            if (ctx.children.get(i).getText().equals("cp-switch")) {
                htmlAttribute.setCP_SWITCH(ctx.CP_SWITCH().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_SWITCH());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCpSwitchExpression(visitCp_switch_expression(ctx.cp_switch_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("cp-switch-case")) {
                htmlAttribute.setCP_SWITCH_CASE(ctx.CP_SWITCH_CASE().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_SWITCH_CASE());
                htmlAttribute.setCP_OPEN_SWITCH_CASE(ctx.CP_OPEN_SWITCH_CASE().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCpSwitchCaseExpression(visitCp_switch_case_expression(ctx.cp_switch_case_expression()));
                htmlAttribute.setCP_CLOSE_SWITCH_CASE(ctx.CP_CLOSE_SWITCH_CASE().toString());
            }
            if (ctx.children.get(i).getText().equals("cp-for")) {
                htmlAttribute.setCP_FOR(ctx.CP_FOR().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_FOR());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCp_for_expression(visitCp_for_expression(ctx.cp_for_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("cp-show")) {
                htmlAttribute.setCP_SHOW(ctx.CP_SHOW().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_SHOW());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCp_show_expression(visitCp_show_expression(ctx.cp_show_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("cp-hide")) {
                htmlAttribute.setCP_HIDE(ctx.CP_HIDE().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_HIDE());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCp_hide_expression(visitCp_hide_expression(ctx.cp_hide_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("cp-if")) {
                htmlAttribute.setCP_IF(ctx.CP_IF().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_IF());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCp_if_expression(visitCp_if_expression(ctx.cp_if_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("cp-model")) {
                htmlAttribute.setCP_MODEL(ctx.CP_MODEL().toString());
                System.out.println("\t\tname: " + htmlAttribute.getCP_MODEL());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCpModelExpression(visitCp_model_expression(ctx.cp_model_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("@")) {
                htmlAttribute.setAT(ctx.AT().toString());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("click")) {
                htmlAttribute.setCLICK(ctx.CLICK().toString());
                System.out.println("\t\tname: " +htmlAttribute.getAT() + htmlAttribute.getCLICK());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setCpClickExpression(visitClick_expression(ctx.click_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("mouseover")) {
                htmlAttribute.setMOUSEOVER(ctx.MOUSEOVER().toString());
                System.out.println("\t\tname: " +htmlAttribute.getAT() + htmlAttribute.getMOUSEOVER());
                htmlAttribute.setCP_OPEN(ctx.CP_OPEN().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setMouseOver_expression(visitMouseover_expression(ctx.mouseover_expression()));
                htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
            }
            if (ctx.children.get(i).getText().equals("type") || ctx.children.get(i).getText().equals("id") || ctx.children.get(i).getText().equals("class") || ctx.children.get(i).getText().equals("data-module")) {
                htmlAttribute.setTAG_NAME(ctx.TAG_NAME().toString());
                System.out.println("\t\tname: " + htmlAttribute.getTAG_NAME());
                htmlAttribute.setTAG_EQUALS(ctx.TAG_EQUALS().toString());
                System.out.print("\t\tvalue: ");
                htmlAttribute.setATTVALUE_VALUE(ctx.ATTVALUE_VALUE().toString());
                System.out.println(htmlAttribute.getATTVALUE_VALUE());

            }
        }


//        htmlAttribute.setCP_APP(ctx.CP_APP().toString());
//        htmlAttribute.setCp_app_expression(visitCp_app_expression(ctx.cp_app_expression()));
//        htmlAttribute.setCpSwitchExpression(visitCp_switch_expression(ctx.cp_switch_expression()));
//        htmlAttribute.setCP_CLOSE(ctx.CP_CLOSE().toString());
//        htmlAttribute.setCP_OPEN_SWITCH_CASE(ctx.CP_OPEN_SWITCH_CASE().toString());
//        htmlAttribute.setCpSwitchCaseExpression(visitCp_switch_case_expression(ctx.cp_switch_case_expression()));
//        htmlAttribute.setCP_CLOSE_SWITCH_CASE(ctx.CP_CLOSE_SWITCH_CASE().toString());

        return htmlAttribute;
    }

    @Override
    public HtmlChardata visitHtmlChardata(HTMLParser.HtmlChardataContext ctx) {
        HtmlChardata htmlChardata = new HtmlChardata();
        htmlChardata.setHTML_TEXT(ctx.HTML_TEXT().toString());
        System.out.print(htmlChardata.getHTML_TEXT());
        htmlChardata.setSEA_WS(ctx.SEA_WS().toString());
        System.out.print(htmlChardata.getSEA_WS());
        return htmlChardata;
    }

    @Override
    public HtmlMisc visitHtmlMisc(HTMLParser.HtmlMiscContext ctx) {
        HtmlMisc htmlMisc = new HtmlMisc();
        htmlMisc.setHtmlComment(visitHtmlComment(ctx.htmlComment()));
        System.out.print(htmlMisc.getHtmlComment());
        htmlMisc.setSEA_WS(ctx.SEA_WS().toString());
        System.out.print(htmlMisc.getSEA_WS());
        return htmlMisc;
    }

    @Override
    public HtmlComment visitHtmlComment(HTMLParser.HtmlCommentContext ctx) {
        HtmlComment htmlComment = new HtmlComment();
        htmlComment.setHTML_COMMENT(ctx.HTML_COMMENT().toString());
        System.out.print(htmlComment.getHTML_COMMENT());
        htmlComment.setHTML_CONDITIONAL_COMMENT(ctx.HTML_CONDITIONAL_COMMENT().toString());
        System.out.print(htmlComment.getHTML_CONDITIONAL_COMMENT());
        return htmlComment;
    }

    @Override
    public Object visitScript(HTMLParser.ScriptContext ctx) {
        return super.visitScript(ctx);
    }

    @Override
    public Object visitStyle(HTMLParser.StyleContext ctx) {
        return super.visitStyle(ctx);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public Cp_If_Expression visitCp_if_expression(HTMLParser.Cp_if_expressionContext ctx) {
        Cp_If_Expression cp_if_expression = new Cp_If_Expression();
        ArrayList<String> SEA_WS_CP = new ArrayList<>();
        System.out.println("size"+ctx.children.size());
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.functionExpression())) {
                System.out.println("apo1");
                cp_if_expression.setFunctionExpression(visitFunctionExpression(ctx.functionExpression()));
//                System.out.print(cp_if_expression.getFunctionExpression().getFunctionParams());
            }
            if (ctx.children.get(i).equals(ctx.variable())) {
                System.out.println("aa");
                cp_if_expression.setVariable(visitVariable(ctx.variable()));
//                System.out.print(cp_if_expression.getVariable().getVaribaleName().getANY_NAME().getANY_NAME());
            }
            if (ctx.children.get(i).equals(ctx.lineCondition())) {
                System.out.println("apo2");
                cp_if_expression.setLineCondition(visitLineCondition(ctx.lineCondition()));
//                System.out.print(cp_if_expression.getLineCondition().getSEA_WS_CP());
            }
            if (ctx.children.get(i).equals(ctx.CONDITIONAL_OPERATORS_TWO_OPERAND())) {
                System.out.println("apo3");
                cp_if_expression.setCONDITIONAL_OPERATORS_TWO_OPERAND(ctx.CONDITIONAL_OPERATORS_TWO_OPERAND().toString());
                System.out.print(cp_if_expression.getCONDITIONAL_OPERATORS_TWO_OPERAND());
            }
            for (int j = 0; j < ctx.SEA_WS_CP().size(); j++) {
                if (ctx.SEA_WS_CP().get(j) != null) {
                    System.out.println("apo4");
                    SEA_WS_CP.add(ctx.SEA_WS_CP(j).toString());
                    System.out.print(cp_if_expression.getSEA_WS_CP().get(j));
                }
            }
            cp_if_expression.setSEA_WS_CP(SEA_WS_CP);
        }
        return cp_if_expression;
    }

    @Override
    public Object visitType_expression(HTMLParser.Type_expressionContext ctx) {
        return super.visitType_expression(ctx);
    }

    @Override
    public Cp_Hide_Expression visitCp_hide_expression(HTMLParser.Cp_hide_expressionContext ctx) {
        Cp_Hide_Expression cp_hide_expression = new Cp_Hide_Expression();
        ArrayList<String> CONDITIONAL_OPERATORS_ONE_OPERAND = new ArrayList<>();
        ArrayList<String> SEA_WS_CP = new ArrayList<>();

        for (int x = 0; x < ctx.children.size(); x++) {
            if (ctx.children.get(x).equals(ctx.functionExpression())) {
                cp_hide_expression.setFunctionExpression(visitFunctionExpression(ctx.functionExpression()));
//                System.out.print(cp_hide_expression.getFunctionExpression().getFunctionParams());
            }
            for (int i = 0; i < ctx.SEA_WS_CP().size(); i++) {
                if (ctx.SEA_WS_CP().get(i) != null) {
                    SEA_WS_CP.add(ctx.SEA_WS_CP(i).toString());
                    //System.out.print(cp_hide_expression.getSEA_WS_CP().get(i));
                }
            }
            cp_hide_expression.setSEA_WS_CP(SEA_WS_CP);
            for (int j = 0; j < ctx.CONDITIONAL_OPERATORS_ONE_OPERAND().size(); j++) {
                if (ctx.CONDITIONAL_OPERATORS_ONE_OPERAND().get(j) != null) {

                    CONDITIONAL_OPERATORS_ONE_OPERAND.add(ctx.CONDITIONAL_OPERATORS_ONE_OPERAND(j).toString());
                    System.out.print(ctx.CONDITIONAL_OPERATORS_ONE_OPERAND(j));
//                    System.out.print(cp_hide_expression.getCONDITIONAL_OPERATORS_ONE_OPERAND());

                }
            }
            cp_hide_expression.setCONDITIONAL_OPERATORS_ONE_OPERAND(CONDITIONAL_OPERATORS_ONE_OPERAND);
        }
        System.out.println();
        return cp_hide_expression;
    }

    @Override
    public FunctionExpression visitFunctionExpression(HTMLParser.FunctionExpressionContext ctx) {
        FunctionExpression functionExpression = new FunctionExpression();

        for (int y = 0; y < ctx.children.size(); y++) {
            if (ctx.children.get(y).equals(ctx.functionName())) {

                functionExpression.setFunctionName(visitFunctionName(ctx.functionName()));
//                System.out.print(functionExpression.getFunctionName().getANY_NAME().getANY_NAME());
            }
            if (ctx.children.get(y).equals(ctx.OPEN_PAR_FUNCTION())) {
                functionExpression.setOPEN_PAR_FUNCTION(ctx.OPEN_PAR_FUNCTION().toString());
                //System.out.print(functionExpression.getOPEN_PAR_FUNCTION());
            }
            if (ctx.children.get(y).equals(ctx.CLOSE_PAR_FUNCTION())) {
                functionExpression.setCLOSE_PAR_FUNCTION(ctx.CLOSE_PAR_FUNCTION().toString());
                //System.out.print(functionExpression.getCLOSE_PAR_FUNCTION());
            }
            ArrayList<String> SEA_WS_CP = new ArrayList<>();
            for (int i = 0; i < ctx.SEA_WS_CP().size(); i++) {
                if (ctx.SEA_WS_CP().get(i) != null) {
                    SEA_WS_CP.add(ctx.SEA_WS_CP(i).toString());
                    //System.out.print(functionExpression.getSEA_WS_CP());
                }
            }
            functionExpression.setSEA_WS_CP(SEA_WS_CP);
            ArrayList<FunctionParams> functionParams = new ArrayList<>();
            for (int j = 0; j < ctx.functionParams().size(); j++) {
                if (ctx.functionParams().get(j) != null) {
                    functionParams.add(visitFunctionParams(ctx.functionParams(j)));
                    //System.out.print(functionExpression.getFunctionParams().get(j).getVariable());
                }
            }
            functionExpression.setFunctionParams(functionParams);
        }
        return functionExpression;
    }

    @Override
    public Cp_Show_Expression visitCp_show_expression(HTMLParser.Cp_show_expressionContext ctx) {
        Cp_Show_Expression cp_show_expression = new Cp_Show_Expression();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.lineCondition1())) {
                cp_show_expression.setLineCondition1(visitLineCondition1(ctx.lineCondition1()));
                // System.out.print(cp_show_expression.getLineCondition1());
            }
            if (ctx.children.get(i).equals(ctx.functionExpression())) {
                cp_show_expression.setFunctionExpression(visitFunctionExpression(ctx.functionExpression()));
//                System.out.print(cp_show_expression.getFunctionExpression().getFunctionParams());
            }
        }
        System.out.println();
        return cp_show_expression;
    }

    @Override
    public MouseOver_Expression visitMouseover_expression(HTMLParser.Mouseover_expressionContext ctx) {
        MouseOver_Expression mouseOver_expression = new MouseOver_Expression();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.functionExpression())) {
                mouseOver_expression.setFunctionExpression(visitFunctionExpression(ctx.functionExpression()));
                //System.out.print(mouseOver_expression.getFunctionExpression().getFunctionParams());
            }
        }
        return mouseOver_expression;
    }

    @Override
    public Click_expression visitClick_expression(HTMLParser.Click_expressionContext ctx) {
        Click_expression click_expression = new Click_expression();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.functionExpression())) {
                click_expression.setFunctionExpression(visitFunctionExpression(ctx.functionExpression()));
                //System.out.print(click_expression.getFunctionExpression().getFunctionParams());
            }
        }
        return click_expression;
    }

    @Override
    public LineCondition visitLineCondition(HTMLParser.LineConditionContext ctx) {
        LineCondition lineCondition = new LineCondition();
        ArrayList<String> SEA_WS_CP = new ArrayList<>();
        for (int z = 0; z < ctx.children.size(); z++) {
            if (ctx.children.get(z).equals(ctx.OBJECT())) {
                lineCondition.setOBJECT(ctx.OBJECT().toString());
                System.out.print(lineCondition.getOBJECT());
            }
            if (ctx.children.get(z).equals(ctx.CONDITIONAL_OPERATORS_TWO_OPERAND())) {
                lineCondition.setCONDITIONAL_OPERATORS_TWO_OPERAND(ctx.CONDITIONAL_OPERATORS_TWO_OPERAND().toString());
                System.out.print(lineCondition.getCONDITIONAL_OPERATORS_TWO_OPERAND());
            }
            if (ctx.children.get(z).equals(ctx.DOT())) {
                lineCondition.setDOT(ctx.DOT().toString());
                System.out.print(lineCondition.getDOT());
            }
            if (ctx.children.get(z).equals(ctx.numericValue())) {
                lineCondition.setNumericValue(visitNumericValue(ctx.numericValue()));
                System.out.print(lineCondition.getNumericValue().getAnyName().getANY_NAME());
            }
            for (int i = 0; i < ctx.SEA_WS_CP().size(); i++) {
                if (ctx.SEA_WS_CP().get(i) != null) {
                    SEA_WS_CP.add(ctx.SEA_WS_CP().toString());
                    System.out.print(lineCondition.getSEA_WS_CP().get(i));
                }
            }
            lineCondition.setSEA_WS_CP(SEA_WS_CP);
            ArrayList<Variable> variables = new ArrayList<>();
            for (int j = 0; j < ctx.variable().size(); j++) {
                if (ctx.variable().get(j) != null) {
                    variables.add(visitVariable(ctx.variable().get(j)));
                    System.out.print(lineCondition.getVariable().getVaribaleName().getANY_NAME().getANY_NAME());
                }
            }
            lineCondition.setVariables(variables);
            ArrayList<String> CONDITIONAL_OPERATORS_ONE_OPERAND = new ArrayList<>();
            for (int y = 0; y < ctx.CONDITIONAL_OPERATORS_ONE_OPERAND().size(); y++) {
                if (ctx.CONDITIONAL_OPERATORS_ONE_OPERAND().get(y) != null) {
                    CONDITIONAL_OPERATORS_ONE_OPERAND.add(ctx.CONDITIONAL_OPERATORS_ONE_OPERAND().toString());
                    System.out.print(lineCondition.getCONDITIONAL_OPERATORS_ONE_OPERAND().get(0));
                }
            }
            lineCondition.setCONDITIONAL_OPERATORS_ONE_OPERAND(CONDITIONAL_OPERATORS_ONE_OPERAND);
        }
        return lineCondition;
    }


    @Override
    public LineCondition1 visitLineCondition1(HTMLParser.LineCondition1Context ctx) {
        LineCondition1 lineCondition1 = new LineCondition1();
        for (int j = 0; j < ctx.children.size(); j++) {
            if (ctx.children.get(j).equals(ctx.OBJECT())) {
                lineCondition1.setOBJECT(ctx.OBJECT().toString());

                //System.out.print(lineCondition1.getOBJECT());
            }
            if (ctx.children.get(j).equals(ctx.DOT())) {
                lineCondition1.setDOT(ctx.DOT().toString());
                //System.out.print(lineCondition1.getDOT());
            }
            if (ctx.children.get(j).equals(ctx.variable())) {
                lineCondition1.setVariable(visitVariable(ctx.variable()));
//                System.out.print(lineCondition1.getVariable().getVaribaleName().getANY_NAME().getANY_NAME());
            }
            if (ctx.SEA_WS_CP() != null)
                for (int i = 0; i < ctx.SEA_WS_CP().size(); i++) {
                    lineCondition1.add_SEW_WS_CP(ctx.SEA_WS_CP().get(i).getSymbol().getText());
                    //System.out.println(lineCondition1.getSEA_WS_CP().get(i));
                }
        }
        return lineCondition1;
    }

    @Override
    public AnyName visitAnyName(HTMLParser.AnyNameContext ctx) {
        AnyName anyName = new AnyName();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.ANY_NAME())) {
                anyName.setANY_NAME(ctx.ANY_NAME().toString());
                //System.out.print(anyName.getANY_NAME());
            }
        }
        return anyName;
    }

    @Override
    public Cp_App_Expression visitCp_app_expression(HTMLParser.Cp_app_expressionContext ctx) {
        Cp_App_Expression cp_app_expression = new Cp_App_Expression();
        for (int i = 0; i < ctx.children.size(); i++) {

            if (ctx.children.get(i).equals(ctx.anyName())) {
                cp_app_expression.setAnyName(visitAnyName(ctx.anyName()));
//                System.out.println(cp_app_expression.getAnyName().getANY_NAME());
            }
        }
        System.out.println();
        return cp_app_expression;
    }


    @Override
    public Cp_For_Expression visitCp_for_expression(HTMLParser.Cp_for_expressionContext ctx) {
        Cp_For_Expression cp_for_expression = new Cp_For_Expression();
        ArrayList<String> SEA_WS_CP = new ArrayList<>();
        for (int z = 0; z < ctx.children.size(); z++) {
            if (ctx.children.get(z).equals(ctx.OBJ())) {
                cp_for_expression.setOBJ(ctx.OBJ().toString());
                //System.out.print(cp_for_expression.getOBJ());
            }
            if (ctx.children.get(z).equals(ctx.COMMA())) {
                cp_for_expression.setCOMMA(ctx.COMMA().toString());
                //System.out.print(cp_for_expression.getCOMMA());
            }
            if (ctx.children.get(z).equals(ctx.INDEX())) {
                cp_for_expression.setINDEX(ctx.INDEX().toString());
                //System.out.print(cp_for_expression.getINDEX());
            }
            if (ctx.children.get(z).equals(ctx.SCOL())) {
                cp_for_expression.setSCOL(ctx.SCOL().toString());
                //System.out.print(cp_for_expression.getSCOL());
            }
            if (ctx.children.get(z).equals(ctx.EQUALS())) {
                cp_for_expression.setEQUALS(ctx.EQUALS().toString());
                //System.out.print(cp_for_expression.getEQUALS());
            }
            if (ctx.children.get(z).equals(ctx.SEA_WS_CP())) {
                cp_for_expression.setSEA_WS_CP(ctx.SEA_WS_CP().toString());
                System.out.print(cp_for_expression.getSEA_WS_CP());
            }
            if (ctx.children.get(z).equals(ctx.OPEN_PAR())) {
                cp_for_expression.setOPEN_PAR(ctx.OPEN_PAR().toString());
                //System.out.print(cp_for_expression.getOPEN_PAR());
            }
            if (ctx.children.get(z).equals(ctx.CLOSE_PAR())) {
                cp_for_expression.setCLOSE_PAR(ctx.CLOSE_PAR().toString());
                //System.out.print(cp_for_expression.getCLOSE_PAR());
            }
            if (ctx.children.get(z).equals(ctx.IN())) {
                cp_for_expression.setIN(ctx.IN().toString());
                System.out.print(cp_for_expression.getIN());
            }
            if (ctx.children.get(z).equals(ctx.SINGLE_QUOTE_STRING_CP())) {
                cp_for_expression.setSINGLE_QUOTE_STRING_CP(ctx.SINGLE_QUOTE_STRING_CP().toString());
                System.out.print(cp_for_expression.getSINGLE_QUOTE_STRING_CP());
            }
            for (int i = 0; i < ctx.SEA_WS_CP().size(); i++) {
                if (ctx.SEA_WS_CP().get(i) != null) {
                    SEA_WS_CP.add(ctx.SEA_WS_CP(i).toString());
                    System.out.print(cp_for_expression.getSEA_WS_CP());
                }
            }
            cp_for_expression.setSEA_WS_CP_List(SEA_WS_CP);
            ArrayList<Array> arrays = new ArrayList<>();
            for (int j = 0; j < ctx.array().size(); j++) {
                if (ctx.array().get(j) != null)
                    arrays.add(visitArray(ctx.array().get(j)));
                //System.out.print(cp_for_expression.getArray().getANY_NAME());
            }
            cp_for_expression.setArrays(arrays);
//            System.out.print(cp_for_expression.getArrays().get(0));
            ArrayList<Variable> variables = new ArrayList<>();
            if (ctx.variable() != null)
                for (int x = 0; x < ctx.variable().size(); x++) {
                    if (ctx.variable().get(x) != null)
                        variables.add(visitVariable(ctx.variable(x)));
                    //System.out.print(cp_for_expression.getVariables());
                }
            cp_for_expression.setVariables(variables);
            System.out.print(cp_for_expression.getVariables().get(0).getVaribaleName().getANY_NAME().getANY_NAME());
        }
        return cp_for_expression;
    }

    @Override
    public cpSwitchCaseExpression visitCp_switch_case_expression(HTMLParser.Cp_switch_case_expressionContext ctx) {
        cpSwitchCaseExpression cpSwitchCaseExpression = new cpSwitchCaseExpression();

        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.EXPRESSION())) {
                cpSwitchCaseExpression.setExpression(ctx.EXPRESSION().toString());
                System.out.print(cpSwitchCaseExpression.getExpression());
            }
            if (ctx.children.get(i).equals(ctx.MATCH())) {
                cpSwitchCaseExpression.setMatch(ctx.MATCH().toString());
                System.out.print(cpSwitchCaseExpression.getMatch());
            }
            if (ctx.children.get(i).equals(ctx.NUMERIC_LITERAL_SWITCH_CASE())) {
                cpSwitchCaseExpression.setNUMERIC_LITERAL_SWITCH_CASE(ctx.NUMERIC_LITERAL_SWITCH_CASE().toString());
                System.out.print(cpSwitchCaseExpression.getNUMERIC_LITERAL_SWITCH_CASE());
            }
            if (ctx.children.get(i).equals(ctx.UNDERSCORE())) {
                cpSwitchCaseExpression.setUnderscore(ctx.UNDERSCORE().toString());
                System.out.print(cpSwitchCaseExpression.getUnderscore());
            }
            if (ctx.children.get(i).equals(ctx.SEA_WS_SWITCH_CASE()))
                if (ctx.SEA_WS_SWITCH_CASE() != null) {
                    for (int j = 0; j < ctx.SEA_WS_SWITCH_CASE().size(); j++) {
                        cpSwitchCaseExpression.add_SEA_WS_SWITCH_CASE(ctx.SEA_WS_SWITCH_CASE(j).getSymbol().getText());
                        System.out.print(cpSwitchCaseExpression.getSEA_WS_SWITCH_CASE().get(j));
                    }
                }

        }
        System.out.println();
        return cpSwitchCaseExpression;
    }

    @Override
    public cpSwitchExpression visitCp_switch_expression(HTMLParser.Cp_switch_expressionContext ctx) {
        cpSwitchExpression cpSwitchExpression = new cpSwitchExpression();
        for (int i = 0; i < ctx.children.size(); i++) {

            if (ctx.children.get(i).equals(ctx.OBJECT())) {
                cpSwitchExpression.setObject(ctx.OBJECT().toString());
                System.out.print(cpSwitchExpression.getObject());
            }
            if (ctx.children.get(i).equals(ctx.DOT())) {
                cpSwitchExpression.setDot(ctx.DOT().toString());
                System.out.print(cpSwitchExpression.getDot());
            }
            if (ctx.children.get(i).equals(ctx.VALUE())) {
                cpSwitchExpression.setValue(ctx.VALUE().toString());
                System.out.print(cpSwitchExpression.getValue());
            }
            if (ctx.SEA_WS_CP() != null) {
                for (int j = 0; j < ctx.SEA_WS_CP().size(); j++) {
                    cpSwitchExpression.add_SEA_WS_CP(ctx.SEA_WS_CP(j).getSymbol().getText());
                    System.out.print(cpSwitchExpression.getSEA_WS_CP().get(j));
                }

            }
        }
        System.out.println();
        return cpSwitchExpression;
    }

    @Override
    public Cp_model_expression visitCp_model_expression(HTMLParser.Cp_model_expressionContext ctx) {
        Cp_model_expression cpModelExpression = new Cp_model_expression();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.variable())) {
                cpModelExpression.setVariable(visitVariable(ctx.variable()));
//                System.out.print(cpModelExpression.getVariable().getVaribaleName().getANY_NAME().getANY_NAME());
            }
        }
        System.out.println();
        return cpModelExpression;
    }

    @Override
    public Object visitExpression(HTMLParser.ExpressionContext ctx) {
        return super.visitExpression(ctx);
    }

    @Override
    public FunctionParams visitFunctionParams(HTMLParser.FunctionParamsContext ctx) {
        FunctionParams functionParams = new FunctionParams();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.COMMA()))
                functionParams.setCOMMA(ctx.COMMA().toString());
            //System.out.print(functionParams.getCOMMA());
            if (ctx.variable() != null)
                for (int j = 0; j < ctx.variable().size(); j++) {
                    functionParams.add_variable(visitVariable(ctx.variable().get(j)));
                    //System.out.print(functionParams.getVariable().get(i));
                }
        }
        return functionParams;
    }

    @Override
    public Value visitValue(HTMLParser.ValueContext ctx) {
        Value value = new Value();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.IDENTIFIER())) {
                value.setIDENTIFIER(ctx.IDENTIFIER().toString());
                //System.out.print(value.getIDENTIFIER());
            }
            if (ctx.children.get(i).equals(ctx.NUMERIC_LITERAL())) {
                value.setNUMERIC_LITERAL(ctx.NUMERIC_LITERAL().toString());
                //System.out.print(value.getNUMERIC_LITERAL());
            }
            if (ctx.children.get(i).equals(ctx.STRING_LITERAL())) {
                value.setSTRING_LITERAL(ctx.STRING_LITERAL().toString());
                //System.out.print(value.getSTRING_LITERAL());
            }
        }
        return value;
    }

    @Override

    public Variable visitVariable(HTMLParser.VariableContext ctx) {
        Variable variable = new Variable();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.varibaleName())) {
                variable.setVaribaleName(visitVaribaleName(ctx.varibaleName()));
                System.out.print(variable.getVaribaleName().getANY_NAME());
            }
        }
        return variable;
    }

    @Override
    public NumericValue visitNumericValue(HTMLParser.NumericValueContext ctx) {
        NumericValue numericValue = new NumericValue();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.anyName())) {
                numericValue.setAnyName(visitAnyName(ctx.anyName()));
                //System.out.print(numericValue.getAnyName().getANY_NAME());
            }
        }
        return numericValue;
    }

    @Override
    public FunctionName visitFunctionName(HTMLParser.FunctionNameContext ctx) {
        FunctionName functionName = new FunctionName();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.anyName())) {
                functionName.setANY_NAME(visitAnyName(ctx.anyName()));
//                System.out.print(functionName.getANY_NAME().getANY_NAME());
            }
        }
        return functionName;
    }

    @Override
    public Array visitArray(HTMLParser.ArrayContext ctx) {
        Array array = new Array();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.anyName())) {
                array.setANY_NAME(ctx.anyName().toString());
                //System.out.print(array.getANY_NAME());
            }
        }
        return array;
    }

    @Override
    public VaribaleName visitVaribaleName(HTMLParser.VaribaleNameContext ctx) {
        VaribaleName varibaleName = new VaribaleName();
        for (int i = 0; i < ctx.children.size(); i++) {
            if (ctx.children.get(i).equals(ctx.SEA_WS())) {
                varibaleName.setSEA_WS(Collections.singletonList(ctx.SEA_WS().toString()));
                //System.out.print(varibaleName.getSEA_WS());
            }
            if (ctx.children.get(i).equals(ctx.anyName())) {
                varibaleName.setANY_NAME(visitAnyName(ctx.anyName()));
                System.out.print(varibaleName.getANY_NAME());
            }
        }
        return varibaleName;
    }
}
