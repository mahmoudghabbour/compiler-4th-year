package ast.nodes;

public class FunctionName extends Node {
    AnyName anyName;

    public void setANY_NAME(AnyName any_name) {
        this.anyName = any_name;
    }

    public AnyName getANY_NAME() {
        return anyName;
    }
}
