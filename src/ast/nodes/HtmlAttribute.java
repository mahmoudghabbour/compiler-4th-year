package ast.nodes;

import ast.CP.*;

import ast.CP.*;
import ast.CP.Cp_App_Expression;
import ast.CP.cpSwitchCaseExpression;
import ast.CP.cpSwitchExpression;


public class HtmlAttribute extends Attribute {

    String CP_APP, CP_SWITCH, CP_SWITCH_CASE, CP_SWITCHDEFAULT, CP_OPEN, CP_CLOSE, CP_OPEN_SWITCH_CASE, CP_CLOSE_SWITCH_CASE, AT, CLICK, MOUSEOVER;
    String CP_MODEL;
    String TAG_NAME;
    String TAG_EQUALS;
    String ATTVALUE_VALUE;
    String CP_SHOW, CP_FOR, CP_HIDE, CP_IF;
    Cp_Show_Expression cp_show_expression;
    Cp_For_Expression cp_for_expression;
    Cp_Hide_Expression cp_hide_expression;
    Cp_If_Expression cp_if_expression;
    Cp_App_Expression cp_app_expression;
    cpSwitchExpression cpSwitchExpression;
    cpSwitchCaseExpression cpSwitchCaseExpression;
    Cp_model_expression cpModelExpression;
    Click_expression cpClickExpression;
    MouseOver_Expression mouseOver_expression;
    public String getTAG_NAME() {
        return TAG_NAME;
    }

    public void setTAG_NAME(String TAG_NAME) {
        this.TAG_NAME = TAG_NAME;
    }

    public String getTAG_EQUALS() {
        return TAG_EQUALS;
    }

    public void setTAG_EQUALS(String TAG_EQUALS) {
        this.TAG_EQUALS = TAG_EQUALS;
    }

    public String getATTVALUE_VALUE() {
        return ATTVALUE_VALUE;
    }

    public void setATTVALUE_VALUE(String ATTVALUE_VALUE) {
        this.ATTVALUE_VALUE = ATTVALUE_VALUE;
    }



    public String getCP_SWITCH() {
        return CP_SWITCH;
    }

    public Cp_Show_Expression getCp_show_expression() {
        return cp_show_expression;
    }

    public Cp_App_Expression getCp_app_expression() {
        return cp_app_expression;
    }


    public String getCP_MODEL() {
        return CP_MODEL;
    }

    public void setCP_MODEL(String CP_MODEL) {
        this.CP_MODEL = CP_MODEL;
    }

    public void setMouseOver_expression(MouseOver_Expression mouseOver_expression) {
        this.mouseOver_expression = mouseOver_expression;
    }

    public MouseOver_Expression getMouseOver_expression() {
        return mouseOver_expression;
    }

    public void setCpClickExpression(Click_expression cpClickExpression) {
        this.cpClickExpression = cpClickExpression;
    }

    public String getCLICK() {
        return CLICK;
    }

    public String getAT() {
        return AT;
    }

    public String getMOUSEOVER() {
        return MOUSEOVER;
    }

    public void setAT(String AT) {
        this.AT = AT;
    }

    public void setCLICK(String CLICK) {
        this.CLICK = CLICK;
    }

    public void setMOUSEOVER(String MOUSEOVER) {
        this.MOUSEOVER = MOUSEOVER;
    }

    public Click_expression getCpClickExpression() {
        return cpClickExpression;
    }


    public String getCP_IF() {
        return CP_IF;
    }

    public Cp_If_Expression getCp_if_expression() {
        return cp_if_expression;
    }

    public void setCP_IF(String CP_IF) {
        this.CP_IF = CP_IF;
    }

    public void setCp_if_expression(Cp_If_Expression cp_if_expression) {
        this.cp_if_expression = cp_if_expression;
    }

    public String getCP_HIDE() {
        return CP_HIDE;
    }

    public Cp_Hide_Expression getCp_hide_expression() {
        return cp_hide_expression;
    }

    public void setCP_HIDE(String CP_HIDE) {
        this.CP_HIDE = CP_HIDE;
    }

    public void setCp_hide_expression(Cp_Hide_Expression cp_hide_expression) {
        this.cp_hide_expression = cp_hide_expression;
    }

    public String getCP_FOR() {
        return CP_FOR;
    }

    public Cp_For_Expression getCp_for_expression() {
        return cp_for_expression;
    }

    public void setCP_FOR(String CP_FOR) {
        this.CP_FOR = CP_FOR;
    }

    public void setCp_for_expression(Cp_For_Expression cp_for_expression) {
        this.cp_for_expression = cp_for_expression;
    }

    public void setCp_show_expression(Cp_Show_Expression cp_show_expression) {
        this.cp_show_expression = cp_show_expression;
    }

    public String getCP_SHOW() {
        return CP_SHOW;
    }

    public void setCP_SHOW(String CP_SHOW) {
        this.CP_SHOW = CP_SHOW;

    }

    public void setCP_SWITCH(String CP_SWITCH) {
        this.CP_SWITCH = CP_SWITCH;
    }

    public String getCP_SWITCH_CASE() {
        return CP_SWITCH_CASE;
    }

    public void setCP_SWITCH_CASE(String CP_SWITCH_CASE) {
        this.CP_SWITCH_CASE = CP_SWITCH_CASE;
    }

    public String getCP_SWITCHDEFAULT() {
        return CP_SWITCHDEFAULT;
    }

    public void setCP_SWITCHDEFAULT(String CP_SWITCHDEFAULT) {
        this.CP_SWITCHDEFAULT = CP_SWITCHDEFAULT;
    }

    public String getCP_OPEN_SWITCH_CASE() {
        return CP_OPEN_SWITCH_CASE;
    }

    public void setCP_OPEN_SWITCH_CASE(String CP_OPEN_SWITCH_CASE) {
        this.CP_OPEN_SWITCH_CASE = CP_OPEN_SWITCH_CASE;
    }

    public String getCP_CLOSE_SWITCH_CASE() {
        return CP_CLOSE_SWITCH_CASE;
    }

    public void setCP_CLOSE_SWITCH_CASE(String CP_CLOSE_SWITCH_CASE) {
        this.CP_CLOSE_SWITCH_CASE = CP_CLOSE_SWITCH_CASE;
    }


    public cpSwitchCaseExpression getCpSwitchCaseExpression() {
        return cpSwitchCaseExpression;
    }

    public void setCpSwitchCaseExpression(ast.CP.cpSwitchCaseExpression cpSwitchCaseExpression) {
        this.cpSwitchCaseExpression = cpSwitchCaseExpression;
    }

    public Cp_model_expression getCpModelExpression() {
        return cpModelExpression;
    }

    public void setCpModelExpression(Cp_model_expression cpModelExpression1) {
        this.cpModelExpression = cpModelExpression1;
    }

    public cpSwitchExpression getCpSwitchExpression() {
        return cpSwitchExpression;
    }

    public void setCpSwitchExpression(cpSwitchExpression cpSwitchExpression) {
        this.cpSwitchExpression = cpSwitchExpression;
    }

    public void setCP_APP(String CP_APP) {
        this.CP_APP = CP_APP;
    }

    public void setCP_OPEN(String CP_OPEN) {
        this.CP_OPEN = CP_OPEN;
    }

    public void setCP_CLOSE(String CP_CLOSE) {
        this.CP_CLOSE = CP_CLOSE;
    }

    public void setCp_app_expression(Cp_App_Expression cp_app_expression) {
        this.cp_app_expression = cp_app_expression;
    }

    public String getCP_APP() {
        return CP_APP;
    }

    public String getCP_OPEN() {
        return CP_OPEN;
    }

    public String getCP_CLOSE() {
        return CP_CLOSE;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
