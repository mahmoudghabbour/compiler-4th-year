package ast.nodes;

public class Array extends Node {
    private String ANY_NAME;

    public void setANY_NAME(String ANY_NAME) {
        this.ANY_NAME = ANY_NAME;
    }

    public String getANY_NAME() {
        return ANY_NAME;
    }
}
