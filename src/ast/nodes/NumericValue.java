package ast.nodes;

public class NumericValue extends Node {
    AnyName anyName;

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public AnyName getAnyName() {
        return anyName;
    }
}
