package ast.nodes;

public class HtmlChardata extends Node {
    String HTML_TEXT;
    String SEA_WS;

    public void setSEA_WS(String SEA_WS) {
        this.SEA_WS = SEA_WS;
    }

    public void setHTML_TEXT(String HTML_TEXT) {
        this.HTML_TEXT = HTML_TEXT;
    }

    public String getHTML_TEXT() {
        return HTML_TEXT;
    }

    public String getSEA_WS() {
        return SEA_WS;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
