package ast.nodes;

public class HtmlMisc extends Node {
     HtmlComment htmlComment;
     String SEA_WS;

    public HtmlComment getHtmlComment() {
        return htmlComment;
    }

    public void setHtmlComment(HtmlComment htmlComment) {
        this.htmlComment = htmlComment;
    }

    public String getSEA_WS() {
        return SEA_WS;
    }

    public void setSEA_WS(String SEA_WS) {
        this.SEA_WS = SEA_WS;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
