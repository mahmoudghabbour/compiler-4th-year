package ast.nodes;

public class ScriptletOrSeaWs extends Node{

    String SCRIPTLET;
    String SEA_WS;

    public void setSCRIPTLET(String SCRIPTLET) {
        this.SCRIPTLET = SCRIPTLET;
    }
    public void setSEA_WS(String SEA_WS){
        this.SEA_WS = SEA_WS;
    }
}
