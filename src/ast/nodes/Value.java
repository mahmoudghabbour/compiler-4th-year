package ast.nodes;

public class Value extends Node {
    String DIGIT;
    String NUMERIC_LITERAL;
    String STRING_LITERAL;
    String IDENTIFIER;

    public void setDIGIT(String DIGIT) {
        this.DIGIT = DIGIT;
    }

    public String getNUMERIC_LITERAL() {
        return NUMERIC_LITERAL;
    }

    public String getSTRING_LITERAL() {
        return STRING_LITERAL;
    }

    public String getIDENTIFIER() {
        return IDENTIFIER;
    }

    public void setNUMERIC_LITERAL(String NUMERIC_LITERAL) {
        this.NUMERIC_LITERAL = NUMERIC_LITERAL;
    }

    public void setSTRING_LITERAL(String STRING_LITERAL) {
        this.STRING_LITERAL = STRING_LITERAL;
    }

    public void setIDENTIFIER(String IDENTIFIER) {
        this.IDENTIFIER = IDENTIFIER;
    }
}
