package ast.nodes;

import java.util.ArrayList;
import java.util.List;

public class HtmlDocument extends Node {
    private List<HTMLElement> elements = new ArrayList<HTMLElement>();
    private List<ScriptletOrSeaWs> scriptletOrSeaWs = new ArrayList<>();
    private String DTD;
    public void setElements(List<HTMLElement> elements) {
        this.elements = elements;
    }

    public List<HTMLElement> getElements() {
        return elements;
    }
    public List<ScriptletOrSeaWs> getScriptletOrSeaWs(){
        return scriptletOrSeaWs;
    }

    public void setDTD(String DTD) {
        this.DTD = DTD;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
