package ast.nodes;

import java.util.ArrayList;
import java.util.List;

public class VaribaleName extends Node {
    List<String> SEA_WS = new ArrayList<>();
    AnyName anyName;

    public void setSEA_WS(List<String> SEA_WS) {
        this.SEA_WS = SEA_WS;
    }

    public void setANY_NAME(AnyName any_name) {
        this.anyName = any_name;
    }

    public List<String> getSEA_WS() {
        return SEA_WS;
    }

    public AnyName getANY_NAME() {
        return anyName;
    }
}
