package ast.nodes;

import java.util.ArrayList;
import java.util.List;

public class HTMLElement extends HtmlElements {
    private String tagOpen, tagName, tagClose, tagSlash, tagSlashClose, scriptLet;
    private HtmlContent htmlContent;
    private ArrayList<HtmlAttribute> htmlAttribute;
    private Script script;
    private Style style;

    public ArrayList<HtmlAttribute> getHtmlAttribute() {
        return htmlAttribute;
    }

    public void setHtmlAttribute(ArrayList<HtmlAttribute> htmlAttribute) {
        this.htmlAttribute = htmlAttribute;
    }


    public void setTagOpen(String tagOpen) {
        this.tagOpen = tagOpen;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public void setTagClose(String tagClose) {
        this.tagClose = tagClose;
    }

    public void setTagSlash(String tagSlash) {
        this.tagSlash = tagSlash;
    }

    public void setTagSlashClose(String tagSlashClose) {
        this.tagSlashClose = tagSlashClose;
    }

    public void setScriptLet(String scriptLet) {
        this.scriptLet = scriptLet;
    }

    public void setHtmlContent(HtmlContent htmlContent) {
        this.htmlContent = htmlContent;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return "Tag_Open\t" + tagOpen +"\n"+ "tagName" + tagName;
    }
}
