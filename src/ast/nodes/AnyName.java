package ast.nodes;

public class AnyName extends Node {
    private String ANY_NAME;

    public void setANY_NAME(String ANY_NAME) {
        this.ANY_NAME = ANY_NAME;
    }

    public String getANY_NAME() {
        return ANY_NAME;
    }
}
