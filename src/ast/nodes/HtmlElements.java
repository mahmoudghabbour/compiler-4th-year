package ast.nodes;

import java.util.List;

public class HtmlElements extends Node{
    List<HtmlMisc> htmlMisc;
    HTMLElement htmlElement;

    public List<HtmlMisc> getHtmlMisc() {
        return htmlMisc;
    }

    public HTMLElement getHtmlElement() {
        return htmlElement;
    }

    public void setHtmlMisc(List<HtmlMisc> htmlMisc) {
        this.htmlMisc = htmlMisc;
    }

    public void setHtmlElement(HTMLElement htmlElement) {
        this.htmlElement = htmlElement;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
