package ast.nodes;

import java.util.List;

public class HtmlContent extends Node {
    HtmlChardata htmlChardata;
    HTMLElement htmlElement;
    HtmlComment htmlComment;
    String CDATA;


    public void setHtmlChardata(HtmlChardata htmlChardata) {
        this.htmlChardata = htmlChardata;
    }

    public void setHtmlElement(HTMLElement htmlElement) {
        this.htmlElement = htmlElement;
    }

    public void setHtmlComment(HtmlComment htmlComment) {
        this.htmlComment = htmlComment;
    }

    public void setCDATA(String CDATA) {
        this.CDATA = CDATA;
    }

    public HtmlChardata getHtmlChardata() {
        return htmlChardata;
    }

    public HTMLElement getHtmlElement() {
        return htmlElement;
    }

    public HtmlComment getHtmlComment() {
        return htmlComment;
    }

    public String getCDATA() {
        return CDATA;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
