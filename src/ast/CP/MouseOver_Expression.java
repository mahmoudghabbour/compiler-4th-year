package ast.CP;

import ast.nodes.Node;

public class MouseOver_Expression extends Node {
    FunctionExpression functionExpression;

    public void setFunctionExpression(FunctionExpression functionExpressionl) {
        this.functionExpression = functionExpressionl;
    }

    public FunctionExpression getFunctionExpression() {
        return functionExpression;
    }
}
