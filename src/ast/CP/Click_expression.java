package ast.CP;

import ast.nodes.*;

public class Click_expression extends Node {
FunctionExpression functionExpression;

    public void setFunctionExpression(FunctionExpression functionExpressionl) {
        this.functionExpression = functionExpressionl;
    }

    public FunctionExpression getFunctionExpression() {
        return functionExpression;
    }
}
