package ast.CP;

import ast.nodes.AnyName;
import ast.nodes.Node;
import ast.nodes.Variable;
public class Cp_model_expression extends Node {
    Variable variable;
    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public Variable getVariable() {
        return variable;
    }

}
