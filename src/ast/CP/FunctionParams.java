package ast.CP;

import ast.nodes.Node;
import ast.nodes.Variable;

import java.util.ArrayList;

public class FunctionParams extends Node {

    ArrayList<Variable> variable = new ArrayList<>();
    String COMMA;

    public void setVariable(ArrayList<Variable> variable) {
        this.variable = variable;
    }

    public void setCOMMA(String COMMA) {
        this.COMMA = COMMA;
    }

    public ArrayList<Variable> getVariable() {
        return variable;
    }

    public String getCOMMA() {
        return COMMA;
    }
    public void add_variable( Variable id){
        variable.add(id);
    }
}
