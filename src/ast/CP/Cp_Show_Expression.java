package ast.CP;

import ast.nodes.Node;

public class Cp_Show_Expression extends Node {

    LineCondition1 lineCondition1;
    FunctionExpression functionExpression;

    public void setFunctionExpression(FunctionExpression functionExpression) {
        this.functionExpression = functionExpression;
    }

    public void setLineCondition1(LineCondition1 lineCondition1) {
        this.lineCondition1 = lineCondition1;
    }

    public LineCondition1 getLineCondition1() {
        return lineCondition1;
    }

    public FunctionExpression getFunctionExpression() {
        return functionExpression;
    }
}
