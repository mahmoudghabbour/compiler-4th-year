package ast.CP;

import ast.nodes.Array;
import ast.nodes.Node;

import java.util.ArrayList;

public class Cp_Hide_Expression extends Node {

    FunctionExpression functionExpression;
    ArrayList<String> CONDITIONAL_OPERATORS_ONE_OPERAND = new ArrayList<>();
    ArrayList<String> SEA_WS_CP = new ArrayList<>();

    public void setFunctionExpression(FunctionExpression functionExpression) {
        this.functionExpression = functionExpression;
    }

    public void setCONDITIONAL_OPERATORS_ONE_OPERAND(ArrayList<String> CONDITIONAL_OPERATORS_ONE_OPERAND) {
        this.CONDITIONAL_OPERATORS_ONE_OPERAND = CONDITIONAL_OPERATORS_ONE_OPERAND;
    }

    public void setSEA_WS_CP(ArrayList<String> SEA_WS_CP) {
        this.SEA_WS_CP = SEA_WS_CP;
    }

    public FunctionExpression getFunctionExpression() {
        return functionExpression;
    }

    public ArrayList<String> getCONDITIONAL_OPERATORS_ONE_OPERAND() {
        return CONDITIONAL_OPERATORS_ONE_OPERAND;
    }

    public ArrayList<String> getSEA_WS_CP() {
        return SEA_WS_CP;
    }
}
