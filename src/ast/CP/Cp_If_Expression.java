package ast.CP;

import ast.nodes.Node;
import ast.nodes.NumericValue;
import ast.nodes.Value;
import ast.nodes.Variable;

import java.util.ArrayList;

public class Cp_If_Expression extends Node {

    LineCondition lineCondition;
    Variable variable;
    FunctionExpression functionExpression;
    ArrayList<String> SEA_WS_CP = new ArrayList<>();
    String CONDITIONAL_OPERATORS_TWO_OPERAND;
    Value value;

    public void setValue(Value value) {
        this.value = value;
    }

    public void setLineCondition(LineCondition lineCondition) {
        this.lineCondition = lineCondition;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public void setFunctionExpression(FunctionExpression functionExpression) {
        this.functionExpression = functionExpression;
    }

    public void setSEA_WS_CP(ArrayList<String> SEA_WS_CP) {
        this.SEA_WS_CP = SEA_WS_CP;
    }

    public void setCONDITIONAL_OPERATORS_TWO_OPERAND(String CONDITIONAL_OPERATORS_TWO_OPERAND) {
        this.CONDITIONAL_OPERATORS_TWO_OPERAND = CONDITIONAL_OPERATORS_TWO_OPERAND;
    }

    public LineCondition getLineCondition() {
        return lineCondition;
    }

    public Variable getVariable() {
        return variable;
    }

    public FunctionExpression getFunctionExpression() {
        return functionExpression;
    }

    public ArrayList<String> getSEA_WS_CP() {
        return SEA_WS_CP;
    }

    public String getCONDITIONAL_OPERATORS_TWO_OPERAND() {
        return CONDITIONAL_OPERATORS_TWO_OPERAND;
    }
}
