package ast.CP;

import ast.nodes.AnyName;
import ast.nodes.Node;

public class Cp_App_Expression extends Node {

    AnyName anyName;

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public AnyName getAnyName() {
        return anyName;
    }
}
