package ast.CP;

import ast.nodes.Array;
import ast.nodes.Node;
import ast.nodes.Variable;

import java.util.ArrayList;

public class Cp_For_Expression extends Node {
    ArrayList<String> SEA_WS_CP_List = new ArrayList<>();
    Variable variable;
    ArrayList<Variable> variables = new ArrayList<>();
    String SEA_WS_CP;
    String IN,SCOL;
    String OBJ,ILETTER;
    String EQUALS,INDEX;
    String OPEN_PAR,CLOSE_PAR;
    String SINGLE_QUOTE_STRING_CP;
    Array array;
    ArrayList<Array> arrays = new ArrayList<>();
    String COMMA;

    public void setVariables(ArrayList<Variable> variables) {
        this.variables = variables;
    }

    public void setArrays(ArrayList<Array> arrays) {
        this.arrays = arrays;
    }

    public ArrayList<Variable> getVariables() {
        return variables;
    }

    public ArrayList<Array> getArrays() {
        return arrays;
    }

    public void setSEA_WS_CP_List(ArrayList<String> SEA_WS_CP_List) {
        this.SEA_WS_CP_List = SEA_WS_CP_List;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public void setSEA_WS_CP(String SEA_WS_CP) {
        this.SEA_WS_CP = SEA_WS_CP;
    }

    public void setIN(String IN) {
        this.IN = IN;
    }

    public void setSCOL(String SCOL) {
        this.SCOL = SCOL;
    }

    public void setOBJ(String OBJ) {
        this.OBJ = OBJ;
    }

    public void setILETTER(String ILETTER) {
        this.ILETTER = ILETTER;
    }

    public void setEQUALS(String EQUALS) {
        this.EQUALS = EQUALS;
    }

    public void setINDEX(String INDEX) {
        this.INDEX = INDEX;
    }

    public void setOPEN_PAR(String OPEN_PAR) {
        this.OPEN_PAR = OPEN_PAR;
    }

    public void setCLOSE_PAR(String CLOSE_PAR) {
        this.CLOSE_PAR = CLOSE_PAR;
    }

    public void setSINGLE_QUOTE_STRING_CP(String SINGLE_QUOTE_STRING_CP) {
        this.SINGLE_QUOTE_STRING_CP = SINGLE_QUOTE_STRING_CP;
    }

    public void setArray(Array array) {
        this.array = array;
    }

    public void setCOMMA(String COMMA) {
        this.COMMA = COMMA;
    }

    public ArrayList<String> getSEA_WS_CP_List() {
        return SEA_WS_CP_List;
    }

    public Variable getVariable() {
        return variable;
    }

    public String getSEA_WS_CP() {
        return SEA_WS_CP;
    }

    public String getIN() {
        return IN;
    }

    public String getSCOL() {
        return SCOL;
    }

    public String getOBJ() {
        return OBJ;
    }

    public String getILETTER() {
        return ILETTER;
    }

    public String getEQUALS() {
        return EQUALS;
    }

    public String getINDEX() {
        return INDEX;
    }

    public String getOPEN_PAR() {
        return OPEN_PAR;
    }

    public String getCLOSE_PAR() {
        return CLOSE_PAR;
    }

    public String getSINGLE_QUOTE_STRING_CP() {
        return SINGLE_QUOTE_STRING_CP;
    }

    public Array getArray() {
        return array;
    }

    public String getCOMMA() {
        return COMMA;
    }
}
