package ast.CP;

import ast.nodes.Node;

import java.util.ArrayList;

public class cpSwitchExpression extends Node {
    private ArrayList<String> SEA_WS_CP = new ArrayList<>();
    private String object;
    private String dot;
    private String value;

    public ArrayList<String> getSEA_WS_CP() {
        return SEA_WS_CP;
    }

    public void setSEA_WS_CP(ArrayList<String> SEA_WS_CP) {
        this.SEA_WS_CP = SEA_WS_CP;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getDot() {
        return dot;
    }

    public void setDot(String dot) {
        this.dot = dot;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void add_SEA_WS_CP(String id) {
        SEA_WS_CP.add(id);
    }
}
