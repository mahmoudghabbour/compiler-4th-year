package ast.CP;

import ast.nodes.Node;
import ast.nodes.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LineCondition1 extends Node {
    ArrayList<String> SEA_WS_CP = new ArrayList<>();
    String OBJECT, DOT;
    Variable variable;

    public void setSEA_WS_CP(ArrayList<String> SEA_WS_CP) {
        this.SEA_WS_CP =SEA_WS_CP;
    }

    public void setOBJECT(String OBJECT) {
        this.OBJECT = OBJECT;
    }

    public void setDOT(String DOT) {
        this.DOT = DOT;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public List<String> getSEA_WS_CP() {
        return  SEA_WS_CP;
    }

    public String getOBJECT() {
        return OBJECT;
    }

    public String getDOT() {
        return DOT;
    }

    public Variable getVariable() {
        return variable;
    }
    public void add_SEW_WS_CP(String id){
        SEA_WS_CP.add(id);
    }
}
