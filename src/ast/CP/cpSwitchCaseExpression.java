package ast.CP;

import ast.nodes.Node;

import java.util.ArrayList;

public class cpSwitchCaseExpression extends Node {
    private ArrayList<String> SEA_WS_SWITCH_CASE;
    private String match, underscore, expression, NUMERIC_LITERAL_SWITCH_CASE;

    public ArrayList<String> getSEA_WS_SWITCH_CASE() {
        return SEA_WS_SWITCH_CASE;
    }

    public void setSEA_WS_SWITCH_CASE(ArrayList<String> SEA_WS_SWITCH_CASE) {
        this.SEA_WS_SWITCH_CASE = SEA_WS_SWITCH_CASE;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public String getUnderscore() {
        return underscore;
    }

    public void setUnderscore(String underscore) {
        this.underscore = underscore;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getNUMERIC_LITERAL_SWITCH_CASE() {
        return NUMERIC_LITERAL_SWITCH_CASE;
    }

    public void setNUMERIC_LITERAL_SWITCH_CASE(String NUMERIC_LITERAL_SWITCH_CASE) {
        this.NUMERIC_LITERAL_SWITCH_CASE = NUMERIC_LITERAL_SWITCH_CASE;
    }
    public void add_SEA_WS_SWITCH_CASE(String id){
        SEA_WS_SWITCH_CASE.add(id);
    }
}
