package ast.CP;

import ast.nodes.Array;
import ast.nodes.FunctionName;
import ast.nodes.Node;

import java.util.ArrayList;
import java.util.List;

public class FunctionExpression extends Node {
    ArrayList<String> SEA_WS_CP = new ArrayList<>();
    FunctionName functionName =new FunctionName();
    ArrayList<FunctionParams> functionParams = new ArrayList<>();
    String OPEN_PAR_FUNCTION;
    String CLOSE_PAR_FUNCTION;

    public void setFunctionParams(ArrayList<FunctionParams> functionParams) {
        this.functionParams = functionParams;
    }

    public ArrayList<FunctionParams> getFunctionParams() {
        return functionParams;
    }

    public void setSEA_WS_CP(ArrayList<String> SEA_WS_CP) {
        this.SEA_WS_CP = SEA_WS_CP;
    }

    public void setFunctionName(FunctionName functionName) {
        this.functionName = functionName;
    }

    public void setOPEN_PAR_FUNCTION(String OPEN_PAR_FUNCTION) {
        this.OPEN_PAR_FUNCTION = OPEN_PAR_FUNCTION;
    }

    public void setCLOSE_PAR_FUNCTION(String CLOSE_PAR_FUNCTION) {
        this.CLOSE_PAR_FUNCTION = CLOSE_PAR_FUNCTION;
    }

    public List<String> getSEA_WS_CP() {
        return SEA_WS_CP;
    }

    public FunctionName getFunctionName() {
        return functionName;
    }

    public String getOPEN_PAR_FUNCTION() {
        return OPEN_PAR_FUNCTION;
    }

    public String getCLOSE_PAR_FUNCTION() {
        return CLOSE_PAR_FUNCTION;
    }
    public void add_SEA_WS_XP(String  id){
        SEA_WS_CP.add(id);
    }
    public void add_functionParams(FunctionParams id){
        functionParams.add(id);
    }
}
