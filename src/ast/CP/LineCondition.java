package ast.CP;

import ast.nodes.AnyName;
import ast.nodes.Node;
import ast.nodes.NumericValue;
import ast.nodes.Variable;

import java.util.ArrayList;

public class LineCondition extends Node {
    ArrayList<String> CONDITIONAL_OPERATORS_ONE_OPERAND = new ArrayList<>();
    Variable variable;
    ArrayList<Variable> variables = new ArrayList<>();
    String CONDITIONAL_OPERATORS_TWO_OPERAND;
    NumericValue numericValue;
    ArrayList<String> SEA_WS_CP = new ArrayList<>();
    String OBJECT,DOT;
    AnyName anyName;

    public void setVariables(ArrayList<Variable> variables) {
        this.variables = variables;
    }
    public void setCONDITIONAL_OPERATORS_ONE_OPERAND(ArrayList<String> CONDITIONAL_OPERATORS_ONE_OPERAND) {
        this.CONDITIONAL_OPERATORS_ONE_OPERAND = CONDITIONAL_OPERATORS_ONE_OPERAND;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public void setCONDITIONAL_OPERATORS_TWO_OPERAND(String CONDITIONAL_OPERATORS_TWO_OPERAND) {
        this.CONDITIONAL_OPERATORS_TWO_OPERAND = CONDITIONAL_OPERATORS_TWO_OPERAND;
    }

    public void setNumericValue(NumericValue numericValue) {
        this.numericValue = numericValue;
    }

    public void setSEA_WS_CP(ArrayList<String> SEA_WS_CP) {
        this.SEA_WS_CP = SEA_WS_CP;
    }

    public void setOBJECT(String OBJECT) {
        this.OBJECT = OBJECT;
    }

    public void setDOT(String DOT) {
        this.DOT = DOT;
    }

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public ArrayList<String> getCONDITIONAL_OPERATORS_ONE_OPERAND() {
        return CONDITIONAL_OPERATORS_ONE_OPERAND;
    }

    public Variable getVariable() {
        return variable;
    }

    public String getCONDITIONAL_OPERATORS_TWO_OPERAND() {
        return CONDITIONAL_OPERATORS_TWO_OPERAND;
    }

    public NumericValue getNumericValue() {
        return numericValue;
    }

    public ArrayList<String> getSEA_WS_CP() {
        return SEA_WS_CP;
    }

    public String getOBJECT() {
        return OBJECT;
    }

    public String getDOT() {
        return DOT;
    }

    public AnyName getAnyName() {
        return anyName;
    }
}
