
parser grammar HTMLParser;

options { tokenVocab= HTMLLexer; }

htmlDocument
    : scriptletOrSeaWs* XML? scriptletOrSeaWs* DTD? scriptletOrSeaWs* htmlElements*
    ;

scriptletOrSeaWs
    : SCRIPTLET
    | SEA_WS
    ;

htmlElements
    : htmlMisc* htmlElement htmlMisc*
    ;

htmlElement
    : TAG_OPEN TAG_NAME htmlAttribute*
      (TAG_CLOSE (htmlContent TAG_OPEN TAG_SLASH TAG_NAME TAG_CLOSE)? | TAG_SLASH_CLOSE)
    | SCRIPTLET
    | script
    | style
    ;

htmlContent
    : htmlChardata? ((htmlElement | CDATA | htmlComment) htmlChardata?)*
    ;

htmlAttribute
    : CP_APP CP_OPEN cp_app_expression CP_CLOSE
    | CP_SHOW CP_OPEN cp_show_expression CP_CLOSE
    | CP_FOR CP_OPEN cp_for_expression CP_CLOSE //IDENTIFIER (K_VAR K_IN K_VAR | K_VAR K_IN array | K_VAR K_IN K_OBJECT) IDENTIFIER
    | CP_SWITCH CP_OPEN cp_switch_expression CP_CLOSE
    | CP_SWITCH_CASE CP_OPEN_SWITCH_CASE cp_switch_case_expression CP_CLOSE_SWITCH_CASE
    | CP_SWITCHDEFAULT
    | CP_HIDE CP_OPEN cp_hide_expression CP_CLOSE
    | CP_IF CP_OPEN cp_if_expression CP_CLOSE
    | CP_MODEL CP_OPEN cp_model_expression CP_CLOSE
    | AT CLICK CP_OPEN click_expression CP_CLOSE
    | AT MOUSEOVER CP_OPEN mouseover_expression CP_CLOSE
    | TAG_NAME (TAG_EQUALS ATTVALUE_VALUE)?
    ;

htmlChardata
    : HTML_TEXT
    | SEA_WS
    ;

htmlMisc
    : htmlComment
    | SEA_WS
    ;
expr
 : OPEN_BRACKET*
 | CLOSE_BRACKET*
 | literal_value
 | j_bool_value
 | j_increment_operator
 | unary_operator expr
 | expr EXPR expr
 | expr ( EXPR ) expr
 | EXPR expr EXPR
 | functionExpression EXPR ( expr ) EXPR
 ;

j_one_line_cond: expr EXPR ( expression | expr | j_one_line_cond ) EXPR (expression | expr | j_one_line_cond) ;

literal_value
 : NUMERIC_LITERAL
 | STRING_LITERAL
 ;

j_bool_value : (J_TRUE | J_FALSE);

j_increment_operator : ((anyName J_INCREMENT_OPERATOR) | (J_INCREMENT_OPERATOR anyName)) ;

unary_operator
 : EXPR
 ;

cp_if_expression
    : lineCondition
    | variable
    | functionExpression SEA_WS_CP* CONDITIONAL_OPERATORS_TWO_OPERAND SEA_WS_CP* variable
    | // K_OBJECT DOT K_VAR (GT | LT) value
    ;

type_expression
    : anyName
    ;
cp_hide_expression
    : functionExpression
    | CONDITIONAL_OPERATORS_ONE_OPERAND* SEA_WS_CP* functionExpression
    ;

functionExpression
    : SEA_WS_CP* functionName OPEN_PAR_FUNCTION functionParams* CLOSE_PAR_FUNCTION SEA_WS_CP*
    ;

cp_show_expression
    : lineCondition1
    | functionExpression
    ;

mouseover_expression
    : functionExpression
    ;

click_expression
    : functionExpression
    ;

lineCondition
         :(CONDITIONAL_OPERATORS_ONE_OPERAND? variable) (CONDITIONAL_OPERATORS_TWO_OPERAND
          (CONDITIONAL_OPERATORS_ONE_OPERAND? (variable | numericValue)))?
        // | lineCondition CONDITIONAL_OPERATORS_CONCAT lineCondition
         | SEA_WS_CP* OBJECT DOT variable SEA_WS_CP* CONDITIONAL_OPERATORS_TWO_OPERAND SEA_WS_CP* variable
         | CONDITIONAL_OPERATORS_ONE_OPERAND SEA_WS_CP* anyName
         ;

lineCondition1
    : SEA_WS_CP* OBJECT DOT variable SEA_WS_CP*
    ;
cp_app_expression
    : anyName
    ;
cp_for_expression
    : SEA_WS_CP* variable SEA_WS_CP IN SEA_WS_CP variable SEA_WS_CP*
    | SEA_WS_CP* (variable | OBJ) SEA_WS_CP IN SEA_WS_CP variable SCOL variable EQUALS INDEX SEA_WS_CP*
    | SEA_WS_CP* variable SEA_WS_CP IN SEA_WS_CP OPEN_PAR variable CLOSE_PAR SCOL variable EQUALS INDEX
    | SEA_WS_CP* variable SEA_WS_CP IN SEA_WS_CP OPEN_PAR (SINGLE_QUOTE_STRING_CP) variable (SINGLE_QUOTE_STRING_CP) CLOSE_PAR SCOL variable EQUALS INDEX
    | SEA_WS_CP* variable SEA_WS_CP IN SEA_WS_CP OPEN_PAR (SINGLE_QUOTE_STRING_CP) (SINGLE_QUOTE_STRING_CP) CLOSE_PAR SCOL variable EQUALS INDEX
    | SEA_WS_CP* variable SEA_WS_CP IN SEA_WS_CP OPEN_PAR (array EXPR)* array+ CLOSE_PAR SCOL variable EQUALS INDEX SEA_WS_CP*
    | SEA_WS_CP* variable SEA_WS_CP IN SEA_WS_CP OPEN_PAR (SINGLE_QUOTE_STRING_CP (variable)  SINGLE_QUOTE_STRING_CP COMMA)+ SINGLE_QUOTE_STRING_CP (variable)+ SINGLE_QUOTE_STRING_CP CLOSE_PAR SCOL variable EQUALS INDEX SEA_WS_CP*
    | SEA_WS_CP* variable EXPR variable SEA_WS_CP IN SEA_WS_CP OBJ SEA_WS_CP*
    ;
cp_switch_case_expression
    : SEA_WS_SWITCH_CASE* MATCH UNDERSCORE EXPRESSION UNDERSCORE NUMERIC_LITERAL_SWITCH_CASE SEA_WS_SWITCH_CASE*
    | NUMERIC_LITERAL_SWITCH_CASE
    ;
cp_switch_expression
    : SEA_WS_CP* OBJECT DOT VALUE SEA_WS_CP*
    ;


cp_model_expression
    : variable
    ;

htmlComment
    : HTML_COMMENT
    | HTML_CONDITIONAL_COMMENT
    ;
expression:
//call function , one line condition, variable name (even inside an object) , array element, boolean expression
      cp_for_expression
    | cp_switch_expression
    | cp_switch_case_expression
    | cp_show_expression
    | cp_hide_expression
    | cp_if_expression
    | cp_model_expression
    | click_expression
    | mouseover_expression
    | lineCondition1
    | lineCondition
    | functionExpression
    | array
    | anyName
    | variable
    | varibaleName
    | functionName
    | functionParams
    ;
script
    : SCRIPT_OPEN (SCRIPT_BODY | SCRIPT_SHORT_BODY)
    ;

style
    : STYLE_OPEN (STYLE_BODY | STYLE_SHORT_BODY)
    ;

functionParams
        : variable (COMMA+ variable+)*// TODO: Define the params
        ;

value
    : NUMERIC_LITERAL
    | STRING_LITERAL
    | IDENTIFIER
    ;
anyName
    : ANY_NAME
    ;
variable
    : varibaleName
    ;

numericValue
         : anyName
         ;

functionName
        : anyName
        ;

array
    : anyName
    ;
varibaleName
    : SEA_WS* anyName SEA_WS*
    ;
