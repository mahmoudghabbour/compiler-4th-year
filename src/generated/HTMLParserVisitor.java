// Generated from C:/Users/Mahmoud/Compiler/New folder (2)/compiler-4th-year/src\HTMLParser.g4 by ANTLR 4.8
package generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link HTMLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface HTMLParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlDocument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlDocument(HTMLParser.HtmlDocumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#scriptletOrSeaWs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScriptletOrSeaWs(HTMLParser.ScriptletOrSeaWsContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlElements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlElements(HTMLParser.HtmlElementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlElement(HTMLParser.HtmlElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlContent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlContent(HTMLParser.HtmlContentContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlAttribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlAttribute(HTMLParser.HtmlAttributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlChardata}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlChardata(HTMLParser.HtmlChardataContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlMisc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlMisc(HTMLParser.HtmlMiscContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(HTMLParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#j_one_line_cond}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJ_one_line_cond(HTMLParser.J_one_line_condContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#literal_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value(HTMLParser.Literal_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#j_bool_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJ_bool_value(HTMLParser.J_bool_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#j_increment_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJ_increment_operator(HTMLParser.J_increment_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(HTMLParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_if_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_if_expression(HTMLParser.Cp_if_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#type_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_expression(HTMLParser.Type_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_hide_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_hide_expression(HTMLParser.Cp_hide_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#functionExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionExpression(HTMLParser.FunctionExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_show_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_show_expression(HTMLParser.Cp_show_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#mouseover_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMouseover_expression(HTMLParser.Mouseover_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#click_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClick_expression(HTMLParser.Click_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#lineCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLineCondition(HTMLParser.LineConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#lineCondition1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLineCondition1(HTMLParser.LineCondition1Context ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_app_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_app_expression(HTMLParser.Cp_app_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_for_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_for_expression(HTMLParser.Cp_for_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_switch_case_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_switch_case_expression(HTMLParser.Cp_switch_case_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_switch_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_switch_expression(HTMLParser.Cp_switch_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#cp_model_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_model_expression(HTMLParser.Cp_model_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlComment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlComment(HTMLParser.HtmlCommentContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(HTMLParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#script}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScript(HTMLParser.ScriptContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#style}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStyle(HTMLParser.StyleContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#functionParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParams(HTMLParser.FunctionParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(HTMLParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#anyName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnyName(HTMLParser.AnyNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(HTMLParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#numericValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumericValue(HTMLParser.NumericValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#functionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionName(HTMLParser.FunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray(HTMLParser.ArrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link HTMLParser#varibaleName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVaribaleName(HTMLParser.VaribaleNameContext ctx);
}