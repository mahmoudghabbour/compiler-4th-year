// Generated from C:/Users/Mahmoud/Compiler/New folder (2)/compiler-4th-year/src\HTMLParser.g4 by ANTLR 4.8
package generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HTMLParser}.
 */
public interface HTMLParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlDocument}.
	 * @param ctx the parse tree
	 */
	void enterHtmlDocument(HTMLParser.HtmlDocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlDocument}.
	 * @param ctx the parse tree
	 */
	void exitHtmlDocument(HTMLParser.HtmlDocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#scriptletOrSeaWs}.
	 * @param ctx the parse tree
	 */
	void enterScriptletOrSeaWs(HTMLParser.ScriptletOrSeaWsContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#scriptletOrSeaWs}.
	 * @param ctx the parse tree
	 */
	void exitScriptletOrSeaWs(HTMLParser.ScriptletOrSeaWsContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlElements}.
	 * @param ctx the parse tree
	 */
	void enterHtmlElements(HTMLParser.HtmlElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlElements}.
	 * @param ctx the parse tree
	 */
	void exitHtmlElements(HTMLParser.HtmlElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlElement}.
	 * @param ctx the parse tree
	 */
	void enterHtmlElement(HTMLParser.HtmlElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlElement}.
	 * @param ctx the parse tree
	 */
	void exitHtmlElement(HTMLParser.HtmlElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlContent}.
	 * @param ctx the parse tree
	 */
	void enterHtmlContent(HTMLParser.HtmlContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlContent}.
	 * @param ctx the parse tree
	 */
	void exitHtmlContent(HTMLParser.HtmlContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlAttribute}.
	 * @param ctx the parse tree
	 */
	void enterHtmlAttribute(HTMLParser.HtmlAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlAttribute}.
	 * @param ctx the parse tree
	 */
	void exitHtmlAttribute(HTMLParser.HtmlAttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlChardata}.
	 * @param ctx the parse tree
	 */
	void enterHtmlChardata(HTMLParser.HtmlChardataContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlChardata}.
	 * @param ctx the parse tree
	 */
	void exitHtmlChardata(HTMLParser.HtmlChardataContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlMisc}.
	 * @param ctx the parse tree
	 */
	void enterHtmlMisc(HTMLParser.HtmlMiscContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlMisc}.
	 * @param ctx the parse tree
	 */
	void exitHtmlMisc(HTMLParser.HtmlMiscContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(HTMLParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(HTMLParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#j_one_line_cond}.
	 * @param ctx the parse tree
	 */
	void enterJ_one_line_cond(HTMLParser.J_one_line_condContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#j_one_line_cond}.
	 * @param ctx the parse tree
	 */
	void exitJ_one_line_cond(HTMLParser.J_one_line_condContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(HTMLParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(HTMLParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#j_bool_value}.
	 * @param ctx the parse tree
	 */
	void enterJ_bool_value(HTMLParser.J_bool_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#j_bool_value}.
	 * @param ctx the parse tree
	 */
	void exitJ_bool_value(HTMLParser.J_bool_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#j_increment_operator}.
	 * @param ctx the parse tree
	 */
	void enterJ_increment_operator(HTMLParser.J_increment_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#j_increment_operator}.
	 * @param ctx the parse tree
	 */
	void exitJ_increment_operator(HTMLParser.J_increment_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(HTMLParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(HTMLParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_if_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_if_expression(HTMLParser.Cp_if_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_if_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_if_expression(HTMLParser.Cp_if_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#type_expression}.
	 * @param ctx the parse tree
	 */
	void enterType_expression(HTMLParser.Type_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#type_expression}.
	 * @param ctx the parse tree
	 */
	void exitType_expression(HTMLParser.Type_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_hide_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_hide_expression(HTMLParser.Cp_hide_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_hide_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_hide_expression(HTMLParser.Cp_hide_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#functionExpression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionExpression(HTMLParser.FunctionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#functionExpression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionExpression(HTMLParser.FunctionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_show_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_show_expression(HTMLParser.Cp_show_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_show_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_show_expression(HTMLParser.Cp_show_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#mouseover_expression}.
	 * @param ctx the parse tree
	 */
	void enterMouseover_expression(HTMLParser.Mouseover_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#mouseover_expression}.
	 * @param ctx the parse tree
	 */
	void exitMouseover_expression(HTMLParser.Mouseover_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#click_expression}.
	 * @param ctx the parse tree
	 */
	void enterClick_expression(HTMLParser.Click_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#click_expression}.
	 * @param ctx the parse tree
	 */
	void exitClick_expression(HTMLParser.Click_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#lineCondition}.
	 * @param ctx the parse tree
	 */
	void enterLineCondition(HTMLParser.LineConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#lineCondition}.
	 * @param ctx the parse tree
	 */
	void exitLineCondition(HTMLParser.LineConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#lineCondition1}.
	 * @param ctx the parse tree
	 */
	void enterLineCondition1(HTMLParser.LineCondition1Context ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#lineCondition1}.
	 * @param ctx the parse tree
	 */
	void exitLineCondition1(HTMLParser.LineCondition1Context ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_app_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_app_expression(HTMLParser.Cp_app_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_app_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_app_expression(HTMLParser.Cp_app_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_for_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_for_expression(HTMLParser.Cp_for_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_for_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_for_expression(HTMLParser.Cp_for_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_switch_case_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_switch_case_expression(HTMLParser.Cp_switch_case_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_switch_case_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_switch_case_expression(HTMLParser.Cp_switch_case_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_switch_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_switch_expression(HTMLParser.Cp_switch_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_switch_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_switch_expression(HTMLParser.Cp_switch_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#cp_model_expression}.
	 * @param ctx the parse tree
	 */
	void enterCp_model_expression(HTMLParser.Cp_model_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#cp_model_expression}.
	 * @param ctx the parse tree
	 */
	void exitCp_model_expression(HTMLParser.Cp_model_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlComment}.
	 * @param ctx the parse tree
	 */
	void enterHtmlComment(HTMLParser.HtmlCommentContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlComment}.
	 * @param ctx the parse tree
	 */
	void exitHtmlComment(HTMLParser.HtmlCommentContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(HTMLParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(HTMLParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#script}.
	 * @param ctx the parse tree
	 */
	void enterScript(HTMLParser.ScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#script}.
	 * @param ctx the parse tree
	 */
	void exitScript(HTMLParser.ScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#style}.
	 * @param ctx the parse tree
	 */
	void enterStyle(HTMLParser.StyleContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#style}.
	 * @param ctx the parse tree
	 */
	void exitStyle(HTMLParser.StyleContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#functionParams}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParams(HTMLParser.FunctionParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#functionParams}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParams(HTMLParser.FunctionParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(HTMLParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(HTMLParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#anyName}.
	 * @param ctx the parse tree
	 */
	void enterAnyName(HTMLParser.AnyNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#anyName}.
	 * @param ctx the parse tree
	 */
	void exitAnyName(HTMLParser.AnyNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(HTMLParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(HTMLParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#numericValue}.
	 * @param ctx the parse tree
	 */
	void enterNumericValue(HTMLParser.NumericValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#numericValue}.
	 * @param ctx the parse tree
	 */
	void exitNumericValue(HTMLParser.NumericValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(HTMLParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(HTMLParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#array}.
	 * @param ctx the parse tree
	 */
	void enterArray(HTMLParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#array}.
	 * @param ctx the parse tree
	 */
	void exitArray(HTMLParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link HTMLParser#varibaleName}.
	 * @param ctx the parse tree
	 */
	void enterVaribaleName(HTMLParser.VaribaleNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#varibaleName}.
	 * @param ctx the parse tree
	 */
	void exitVaribaleName(HTMLParser.VaribaleNameContext ctx);
}