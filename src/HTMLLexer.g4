/*
 [The "BSD licence"]
 Copyright (c) 2013 Tom Everett
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

lexer grammar  HTMLLexer;

HTML_COMMENT
    : '<!--' .*? '-->'
    ;

HTML_CONDITIONAL_COMMENT
    : '<![' .*? ']>'
    ;

XML
    : '<?xml' .*? '>'
    ;

CDATA
    : '<![CDATA[' .*? ']]>'
    ;

DTD
    : '<!' .*? '>'
    ;

SCRIPTLET
    : '<?' .*? '?>'
    | '<%' .*? '%>'
    ;

SEA_WS
    :  (' '|'\t'|'\r'? '\n')+
    ;

SCRIPT_OPEN
    : '<script' .*? '>' ->pushMode(SCRIPT)
    ;

STYLE_OPEN
    : '<style' .*? '>'  ->pushMode(STYLE)
    ;

TAG_OPEN
    : '<' -> pushMode(TAG)
    ;

HTML_TEXT
    : ~'<'+
    ;


// tag declarations

mode TAG;

TAG_CLOSE
    : '>' -> popMode
    ;

TAG_SLASH_CLOSE
    : '/>' -> popMode
    ;

TAG_SLASH
    : '/'
    ;


CP_APP
    : 'cp-app' ->pushMode(CP_ATTRIBUTE)
    ;

CP_SHOW
    : 'cp-show' -> pushMode(CP_ATTRIBUTE)
    ;

CP_FOR
    : 'cp-for' -> pushMode(CP_ATTRIBUTE)
    ;

CP_SWITCH
    : 'cp-switch' ->pushMode(CP_ATTRIBUTE)
    ;

CP_SWITCH_CASE
    : 'cp-switch-case' ->pushMode(CP_SWITCH_CASE_MODE)
    ;

CP_SWITCHDEFAULT
    : 'cp-switchDefault'
    ;
CP_HIDE
    :'cp-hide' -> pushMode(CP_ATTRIBUTE)
    ;
CP_IF
    : 'cp-if' -> pushMode(CP_ATTRIBUTE)
    ;

CP_MODEL
    : 'cp-model' -> pushMode(CP_ATTRIBUTE)
    ;
AT
    : '@'
    ;
CLICK
    : 'click' -> pushMode(CP_ATTRIBUTE)
    ;

MOUSEOVER
    : 'mouseover' -> pushMode(CP_ATTRIBUTE)
    ;

// lexing mode for attribute values

TAG_EQUALS
    : '=' -> pushMode(ATTVALUE)
    ;

TAG_NAME
    : TAG_NameStartChar TAG_NameChar*
    ;

TAG_WHITESPACE
    : [ \t\r\n] -> channel(HIDDEN)
    ;

IDENTIFIER
         : '"' (~'"' | '""')* '"'
         | '`' (~'`' | '``')* '`'
         | '[' ~']'* ']'
         | [a-zA-Z_] [a-zA-Z_0-9]* // TODO check: needs more chars in set
         ;

fragment
HEXDIGIT
    : [a-fA-F0-9]
    ;

fragment
DIGIT
    : [0-9]
    ;

fragment
TAG_NameChar
    : TAG_NameStartChar
    | '-'
    | '_'
    | '.'
    | DIGIT
    | '\u00B7'
    | '\u0300'..'\u036F'
    | '\u203F'..'\u2040'
    ;

fragment
TAG_NameStartChar
    : [:a-zA-Z]
    | '\u2070'..'\u218F'
    | '\u2C00'..'\u2FEF'
    | '\u3001'..'\uD7FF'
    | '\uF900'..'\uFDCF'
    | '\uFDF0'..'\uFFFD'
    ;


// <scripts>

mode SCRIPT;

SCRIPT_BODY
    : .*? '</script>' -> popMode
    ;

SCRIPT_SHORT_BODY
    : .*? '</>' -> popMode
    ;


// <styles>

mode STYLE;

STYLE_BODY
    : .*? '</style>' -> popMode
    ;

STYLE_SHORT_BODY
    : .*? '</>' -> popMode
    ;


// attribute values

mode ATTVALUE;

// an attribute value may have spaces b/t the '=' and the value
ATTVALUE_VALUE
    : ' '* ATTRIBUTE -> popMode
    ;

ATTRIBUTE
    : DOUBLE_QUOTE_STRING
    | SINGLE_QUOTE_STRING
    | ATTCHARS
    | HEXCHARS
    | DECCHARS
    ;

fragment ATTCHARS
    : ATTCHAR+ ' '?
    ;

fragment ATTCHAR
    : '-'
    | '_'
    | '.'
    | '/'
    | '+'
    | ','
    | '?'
    | '='
    | ':'
    | ';'
    | '#'
    | [0-9a-zA-Z]
    ;

fragment HEXCHARS
    : '#' [0-9a-fA-F]+
    ;

fragment DECCHARS
    : [0-9]+ '%'?
    ;

fragment DOUBLE_QUOTE_STRING
    : '"' ~[<"]* '"'
    ;

fragment SINGLE_QUOTE_STRING
    : '\'' ~[<']* '\''
    ;

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];

mode CP_SWITCH_CASE_MODE;

EQUALS_SWITCH_CASE
    : '='
    ;

CP_OPEN_SWITCH_CASE
    :   EQUALS_SWITCH_CASE '"' * ->popMode, pushMode(CP_SWITCH_CASE_MODE)
    ;
NUMERIC_LITERAL_SWITCH_CASE
    : DIGIT+
    ;

SEA_WS_SWITCH_CASE
    :  (' '|'\t'|'\r'? '\n')+
    ;

DIGIT_SWITCH_CASE
    : [0-9]
    ;
EXPRESSION
    : 'expression'
    ;
UNDERSCORE : '_';

MATCH
    : 'match'
    ;
CP_CLOSE_SWITCH_CASE
    : '"'  ->popMode
    ;


mode CP_ATTRIBUTE;

EQUALS
    : '='
    ;

CP_OPEN
    :   EQUALS '"' * ->popMode, pushMode(CP_ATTRIBUTE)
    ;

OPEN_BRACKET
    : '{'
    ;

CLOSE_BRACKET
    : '}'
    ;

EXPR
    : '||'
    | '/'
    | '%'
    | '<<'
    | '>>'
    | '+'
    | OPEN_BRACKET
    | CLOSE_BRACKET
    | AND
    | OR
    | LT_EQ
    | GT
    | GT_EQ
    | EQ
    | NOT_EQ1
    | NOT_EQ2
    | COMMA
    | MARK_QA
    | ':'
    | MINS
    | PLUS
    | MDA
    ;
CONDITIONAL_OPERATORS_ONE_OPERAND
    : NOT
    ;

CONDITIONAL_OPERATORS_TWO_OPERAND
    : EQ
    | GT
    | NOT_EQ1
    | LT
    | GT_EQ
    | LT_EQ
    ;

VALUE
    : 'value'
    ;
INDEX
    : 'index'
    ;
//ILETTER
//    : 'i'
//    ;
IN
     : 'in'
     ;


OBJ
    : 'obj'
    ;




SEA_WS_CP
    :  (' '|'\t'|'\r'? '\n')+
    ;

OBJECT
    : 'object'
    ;


ANY_NAME
    : NUMERIC_LITERAL
    | STRING_LITERAL
    | DIGIT
   // | ATTCHAR
    //| IN
    ;

STRING_LITERAL
 : [a-zA-Z_] [a-zA-Z_0-9]*
 | '\'' ( ~'\'' | '\'\'' )* '\''
 ;



DOT : '.';
/*
DOUBLE_QUOTE_STRING_CP
    : '"'+
    ;
*/
SINGLE_QUOTE_STRING_CP
    : '\''+
    ;


GT : '>';


CP_CLOSE
    : '"'  ->popMode
    ;

NUMERIC_LITERAL
 : DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
 | '.' DIGIT+ ( E [-+]? DIGIT+ )?
 ;

SCOL : ';';
NOT: '!';
COMMA : ',';
OPEN_PAR : '[';
CLOSE_PAR : ']';
OPEN_PAR_FUNCTION : '(';
CLOSE_PAR_FUNCTION : ')';
LT_EQ : '<=';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';
LT : '<';
J_TRUE : 'true';
J_FALSE : 'false';
J_INCREMENT_OPERATOR : ('++' | '--');
AND : 'and';
OR : 'or';
PLUS: '+';
MINS: '-';
MDA: '~';
MARK_QA: '?';
/*
//K_CP_FOR: C P '_' F O R;
K_CP_SWITCH: C P '_' S W I T C H;
//K_CP_SWITCH_CASE: C P '_' S W I T C H '_' C A S E;
K_CP_SWITCHDEFAULT: C P '_' S W I T C H D E F A U L T;
K_CP_SHOW: C P '_' S H O W;
K_CP_HIDE: C P '_' H I D E;
K_CP_IF: C P '_' I F;
K_VAR: V A R;
//K_IN: I N;
K_ARRAY: A R R A Y;
K_OBJECT: O B J E C T;
K_VALUE: V A L U E;
K_BRACT: B R A C T;
K_FUNCTION: F U N C T I O N;




LETTERS: [a-zA-Z];
//IN:'in';

//DOT : '.';
OPEN_PAR : '(';
CLOSE_PAR : ')';
OPEN_PAR1 : '[';
CLOSE_PAR2 : ']';
COMMA : ',';
STAR : '*';
PLUS : '+';
MINUS : '-';
TILDE : '~';
PIPE2 : '||';
DIV : '/';
MOD : '%';
LT2 : '<<';
GT2 : '>>';
AMP : '&';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
//GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';
MATCH_EXPRESSION:'match_expression';*/
